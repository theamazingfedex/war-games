package edu.neumont.csc380.hello.exception;

public class NoHelloException extends RuntimeException {
	private static final long serialVersionUID = 1L;

	public NoHelloException(String message) {
		super(message);
	}
}
