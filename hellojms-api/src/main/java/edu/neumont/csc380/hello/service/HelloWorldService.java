package edu.neumont.csc380.hello.service;

import edu.neumont.csc380.hello.model.Echo;
import edu.neumont.csc380.hello.model.Echoed;

public interface HelloWorldService {
	public boolean isPrime(int number);
	public boolean areCoprime(String numbers);
}
