package edu.neumont.csc380.hello.model;

public interface Signable {
	String getSignature();
	String getPublicKey();
}
