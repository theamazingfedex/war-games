package edu.neumont.csc380.hello.model;

import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;
import javax.xml.bind.annotation.XmlSeeAlso;

// we can do inheritance with JAXB, too
@XmlRootElement(name="echo")
@XmlSeeAlso(value={Echo.class})
public class Echo implements Signable {
	@XmlElement
	private String signature;

	@XmlElement
	private String publicKey;
	
	@XmlElement(name="message")
	private String message;
	
	@XmlElement(name="echoCount")
	private int numberOfEchoes;
	
	@XmlElement(name="echoLength")
	private int lengthOfEcho;


	@XmlTransient
	public String getMessage() {
		return message;
	}

	public void setMessage(String message) {
		this.message = message;
	}

	@XmlTransient
	public int getNumberOfEchoes() {
		return numberOfEchoes;
	}

	public void setNumberOfEchoes(int numberOfEchoes) {
		this.numberOfEchoes = numberOfEchoes;
	}

	@XmlTransient
	public int getLengthOfEcho() {
		return lengthOfEcho;
	}

	public void setLengthOfEcho(int lengthOfEcho) {
		this.lengthOfEcho = lengthOfEcho;
	}

	@XmlTransient
	public String getSignature() {
		return signature;
	}

	public void setSignature(String signature) {
		this.signature = signature;
	}
	
	@XmlTransient
	public String getPublicKey() {
		return publicKey;
	}
	
	public void setPublicKey(String publicKey) {
		this.publicKey = publicKey;
	}
}
