package edu.neumont.csc380.hello.jms;

import org.apache.camel.ValidationException;
import org.apache.camel.builder.RouteBuilder;
import org.apache.camel.converter.jaxb.JaxbDataFormat;

import edu.neumont.csc380.hello.exception.NoHelloException;
//import org.apache.camel.model.dataformat.JaxbDataFormat;

public class HelloServerRoutes extends RouteBuilder {

	@Override
	public void configure() throws Exception {
//		JaxbDataFormat jaxb = new JaxbDataFormat("edu.neumont.csc380.hello");
		
//		from("jms:queue:hello").to("helloWorldService");
		from("jms:queue:checkPrime").to("helloWorldService");
		from("jms:queue:checkCoPrime").to("helloWorldService");
//		from("jms:queue:echo")
//			.doTry()
//				// "validator" is a camel component just like "jms", "cxf", "bean", "log", etc.
//				.to("validator:edu/neumont/csc380/hello/hello.xsd")
//				.unmarshal(jaxb)
//				// now we are ready to call methods that require Java objects as parameters
//				.to("authenticationService")
//				.to("helloWorldService")
//				// marshal it back into xml; btw, the client will still work if we take this out.  Why?
//				.marshal(jaxb)
//			.doCatch(ValidationException.class)
//				// why are we using a try/catch? To normalize the reported exception.
//				.throwException(new NoHelloException("No hello for you!"));
	}
}
