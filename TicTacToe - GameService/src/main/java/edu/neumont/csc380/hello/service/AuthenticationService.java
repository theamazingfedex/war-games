package edu.neumont.csc380.hello.service;

import org.springframework.stereotype.Service;

import edu.neumont.csc380.hello.exception.NoHelloException;
import edu.neumont.csc380.hello.model.Signable;

@Service
public class AuthenticationService {
	public Signable isAuthenticated(Signable signable) {
		if ( signable.getSignature().equals("foo") ) {
			return signable;
		}
		
		throw new NoHelloException("No hello for you!");
	}
}
