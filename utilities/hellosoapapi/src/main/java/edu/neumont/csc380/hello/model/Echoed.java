package edu.neumont.csc380.hello.model;

import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;

@XmlRootElement(name="echoed")
public class Echoed {
	
	@XmlAttribute(name="message")
	private String message;
	
	public Echoed() {}
	
	public Echoed(Echo echo)
	{
		String message = echo.getMessage();
		for (int i = 0; i < echo.getLengthOfEcho(); i++)
		{
			message = message.replace("o", "oo");
		}
		this.message = message;
		for (int i = 0; i < echo.getNumberOfEchoes(); i++)
		{
			this.message += " " + message;
		}
	}
	
	@XmlTransient
	public String getMessage()
	{
		return this.message;
	}
	
	public void setMessage(String message)
	{
		this.message = message;
	}
}
