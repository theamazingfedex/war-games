package edu.neumont.csc380.hello.model;

public class KeyPair {

	long key;
	long n;
	
	public KeyPair(long key, long n){
		this.key = key;
		this.n = n;
	}

	public long getKey() {
		return key;
	}

	public void setKey(long key) {
		this.key = key;
	}

	public long getN() {
		return n;
	}

	public void setN(long n) {
		this.n = n;
	}
	
	
}
