package edu.neumont.csc380.hello.exception;

import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.ws.WebFault;

@WebFault(name="NoHelloFault")
@XmlRootElement(name="NoHelloFault")
public class NoHelloException extends RuntimeException{
	public NoHelloException(String message)
	{
		super(message);
	}
}
