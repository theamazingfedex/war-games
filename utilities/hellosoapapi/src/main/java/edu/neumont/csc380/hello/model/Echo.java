package edu.neumont.csc380.hello.model;

import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;

@XmlRootElement(name="echo")
public class Echo {
	
	@XmlElement(name="message")
	private String message;
	
	@XmlElement(name="echoCount")
	private int numberOfEchoes;
	
	@XmlElement(name="echoLength")
	private int lengthOfEcho;
	
	@XmlTransient
	public String getMessage() {
		return message;
	}

	public void setMessage(String message) {
		this.message = message;
	}
	
	@XmlTransient
	public int getNumberOfEchoes() {
		return numberOfEchoes;
	}

	public void setNumberOfEchoes(int numberOfEchoes) {
		this.numberOfEchoes = numberOfEchoes;
	}
	
	@XmlTransient
	public int getLengthOfEcho() {
		return lengthOfEcho;
	}

	public void setLengthOfEcho(int lengthOfEcho) {
		this.lengthOfEcho = lengthOfEcho;
	}
	
	
}
