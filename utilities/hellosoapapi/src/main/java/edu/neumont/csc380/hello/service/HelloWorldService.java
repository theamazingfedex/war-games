package edu.neumont.csc380.hello.service;

import javax.jws.WebMethod;
import javax.jws.WebParam;
import javax.jws.WebService;

import edu.neumont.csc380.hello.model.KeySet;

@WebService
public interface HelloWorldService {
	
	@WebMethod(action="/getKeys")
	KeySet getKeys();
	
	@WebMethod(action="/getKeysWithIdentifier")
	KeySet getKeysWithIdentifier(@WebParam(name="identifier") String identifier);
}
