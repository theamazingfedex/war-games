package edu.neumont.csc380.hello.model;

public class KeySet {
	private String identifier;
	private KeyPair publicKey;
	private KeyPair privateKey;
	
	public KeySet(KeyPair pubKey, KeyPair privKey, String identifier)
	{
		this.identifier = identifier;
		this.publicKey = pubKey;
		this.privateKey = privKey;
	}

	public KeyPair getPublicKey() {
		return publicKey;
	}

	public void setPublicKey(KeyPair publicKey) {
		this.publicKey = publicKey;
	}

	public KeyPair getPrivateKey() {
		return privateKey;
	}

	public void setPrivateKey(KeyPair privateKey) {
		this.privateKey = privateKey;
	}

	public String getIdentifier() {
		return this.identifier;
	}
	
	public void setIdentifier(String identifier) {
		this.identifier = identifier;
	}
	
	
}
