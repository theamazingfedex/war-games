package edu.neumont.csc380.hello.model;

import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement
public class PrimeModel {
	
	public PrimeModel(int number, double confidence, String recall)
	{
		this.number = number;
		this.recall = recall;
		this.confidence = confidence;
	}
	public PrimeModel(){}
	
	public int number = -1;
	
	public String recall;
	
	public double confidence = -1;
}
