package edu.neumont.csc380.hello.service;

import javax.jws.WebService;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;

import edu.neumont.csc380.hello.model.PrimeModel;

@WebService
@Produces({"application/xml"})
@Path("/prime")
public interface PrimeServiceInterface {

	@GET
	@Path("/{number}/{witnesses}")
	public PrimeModel getProbablePrime(@QueryParam("number") String number, @QueryParam("witnesses") String witnesses);
	
	@GET
	@Path("/recall/{number}")
	public PrimeModel getMoreAssurance(@PathParam("number") String number);
	
	@GET
	public PrimeModel getPrime();
}
