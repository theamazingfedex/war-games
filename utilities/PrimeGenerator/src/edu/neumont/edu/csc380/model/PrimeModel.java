package edu.neumont.edu.csc380.model;
import javax.xml.bind.annotation.XmlRootElement;


@XmlRootElement(name="prime")
public class PrimeModel {
	
	public int number;
	
	public String recall;
	
	public double confidence;
}
