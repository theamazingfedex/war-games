package edu.neumont.csc380;

import java.math.BigInteger;
import java.util.InputMismatchException;
import java.util.Random;

import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.Response;
import javax.ws.rs.ext.ExceptionMapper;

import org.springframework.stereotype.Service;

import edu.neumont.edu.csc380.model.PrimeModel;

/**
 * This class is the main service that handles requests and responses from the
 * client. It is responsible for calculating very large probably prime numbers.
 * 
 * @author Jake Heimbouch
 * 
 */

@Service
@Path("/prime")
public class ProbablyPrimeService {

	PrimacyManager pm = new PrimacyManager();

	@GET
	@Path("/")
	@Produces({ "application/xml", "application/json" })
	public PrimeModel getRandomPrime() {
		PrimeModel model = new PrimeModel();
		
		BigInteger bigInt = BigInteger.probablePrime(31, new Random());
		while (!bigInt.isProbablePrime(7))
			bigInt = BigInteger.probablePrime(31, new Random());
		
		model.number = bigInt.intValue();
		model.confidence = 7;
		model.recall = "http://localhost:8080/Enlightenment/prime/" + String.valueOf(model.number);
//		
//		
//		boolean isPrime;
//		BigInteger bi;
//		try {
//			bi = BigInteger.probablePrime(32, new Random());
//			model.number = bi.intValue();
//			model.recall = "http://localhost:8080/Enlightenment/prime/"
//					+ bi.intValue();
//			isPrime = bi.isProbablePrime(1);
//			if (isPrime) {
//				model.confidence = 1;
//			} else {
//				model.confidence = 0;
//			}
//		} catch (Exception e) {
//
//		}
		return model;
	}

	@GET
	@Path("/self")
	@Produces({ "application/xml", "application/json" })
	public PrimeModel getProbablePrime(@QueryParam("number") String number,
			@QueryParam("witnesses") String witnesses) {
		PrimeModel model = new PrimeModel();
		BigInteger bi;
		boolean isPrime = true;
		try {
			int primeNum = Integer.parseInt(number);
			model.number = primeNum;
			bi = new BigInteger(number);
			isPrime = bi.isProbablePrime(1);
			String[] numStrings = witnesses.split(",");
			int[] numbersInts = new int[numStrings.length];
			for (int i = 0; i < numbersInts.length; i++) {
				numbersInts[i] = Integer.parseInt(numStrings[i]);
				if (primeNum % numbersInts[i] == 0) {
					if (numbersInts[i] != 1)
						isPrime = false;
				}
			}
			if (isPrime) {
				model.confidence = 1.0;
			} else {
				model.confidence = 0.0;
			}
			model.recall = "http://localhost:8080/Enlightenment/prime/"
					+ bi.intValue();
		} catch (Exception e) {

		}
		return model;
	}

	@GET
	@Path("/{number}")
	@Produces({ "application/xml", "application/json" })
	public PrimeModel getMoreAssurance(@PathParam("number") String number) {
		PrimeModel pm = new PrimeModel();
		BigInteger bi;
		boolean isPrime = true;
	
			bi = new BigInteger(number);
			isPrime = bi.isProbablePrime(10);
			pm.number = bi.intValue();
			pm.recall = "http://localhost:8080/Enlightenment/prime/"
					+ bi.intValue();
			pm.confidence = 10;
		
		return pm;
	}
}
