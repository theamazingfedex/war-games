package edu.neumont.csc380.filters;

import javax.ws.rs.WebApplicationException;
import javax.ws.rs.core.Response;
import javax.ws.rs.ext.ExceptionMapper;
import javax.ws.rs.ext.Provider;

@Provider
public class SecurityExceptionMapper implements ExceptionMapper<Exception> {

	public Response toResponse(Exception e) {
		if(e instanceof NumberFormatException){
			return Response.status(Response.Status.BAD_REQUEST).build();
		}	
		if(e instanceof WebApplicationException){
			return Response.status(Response.Status.NOT_FOUND).build();
		}
		
		return Response.status(Response.Status.FORBIDDEN).build();
	}
}