package edu.neumont.csc380.soap.util;

import javax.xml.ws.WebFault;

@WebFault
public class FaultMapper extends Exception{
	
	public FaultMapper(String message){
		super("Something bad happened: " + message);
	}
	
//	public void myException(Exception e) throws NullPointerException{
//		e.printStackTrace();
//	}
	
}
