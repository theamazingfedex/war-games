package edu.neumont.csc380.soap.service;

import java.io.IOException;

import javax.security.auth.callback.Callback;
import javax.security.auth.callback.CallbackHandler;
import javax.security.auth.callback.UnsupportedCallbackException;

import org.apache.ws.security.WSPasswordCallback;
import org.springframework.stereotype.Service;

/**
 * Sets the password which the client validates against
 * @author Daniel
 *
 */
@Service("helloCallbackHandler")
public class ServerCallbackHandler implements CallbackHandler {

	public void handle(Callback[] callbacks) throws IOException,
			UnsupportedCallbackException {
		WSPasswordCallback pc = (WSPasswordCallback) callbacks[0];
		
		if("joe".equals(pc.getIdentifier()))
			pc.setPassword("joespassword");
	}
}
