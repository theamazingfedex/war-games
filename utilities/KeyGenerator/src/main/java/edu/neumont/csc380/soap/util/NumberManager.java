package edu.neumont.csc380.soap.util;

public class NumberManager {

	public String[] checkPrime(int[] numbers)
	{
		String[] arePrime = new String[numbers.length];
		for (int n = 0; n < numbers.length; n++)
			if (isPrime(numbers[n] ))
				arePrime[n] = "true";
			else
				arePrime[n] = "false";
		return arePrime;
	}
	// isPrime function accredited to Oscar Sanchez:
	//  http://www.mkyong.com/java/how-to-determine-a-prime-number-in-java/
	public boolean isPrime(int n) {
	    if (n == 2) return true;
	    else if (n%2==0) return false;
	    for(int i=3;i*i<=n;i+=2) {
	        if(n%i==0)
	            return false;
	    }
	    return true;
	}
	// Co Prime and HCF(HighestCommonFactor) functions accredited to Unknown Author:
	//  http://www.cprogramto.com/c-program-to-find-pair-of-numbers-are-relative-prime-co-prime/
	public boolean checkCoPrime(int[] numbers)
	{
		int gcd = -1;
		for (int i = 0; i < numbers.length; i++)
		{
			for (int j = 0; j < numbers.length; j++)
			{
				if (numbers[i] != numbers[j])
					gcd = hcf(numbers[i], numbers[j]);
			}
		}
		if (gcd == 1)
			return true;
		return false;
	}
	int hcf(int a, int h) 
	{
	    int temp; 
	    while (true)
	    {
	        temp = a%h;
	        if (temp==0)
	        return h;
	        a = h;
	        h = temp;
	    }
	} 
}
