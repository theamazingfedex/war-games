package edu.neumont.csc380.soap.service;

import javax.jws.WebMethod;
import javax.jws.WebParam;
import javax.xml.ws.WebFault;

import org.springframework.beans.factory.annotation.Autowired;

import edu.neumont.csc380.hello.model.KeySet;
import edu.neumont.csc380.hello.model.PrimeModel;
import edu.neumont.csc380.hello.service.HelloWorldService;
import edu.neumont.csc380.hello.service.PrimeServiceInterface;
import edu.neumont.csc380.soap.util.RSAGenerator;
/**
 * Implements HelloWorldService
 * @author Daniel
 *	Contains server methods to return a KeySet, containing PublicKey, PrivateKey, and IdentifyingInformation
 */
public class HelloWorldServiceImpl implements HelloWorldService{
	
	@Autowired
	private PrimeServiceInterface primeServiceInterface;
	
	RSAGenerator keyGen = new RSAGenerator();
	
	/**
	 * returns a PublicKey and a PrivateKey
	 */
	@WebMethod(action="/getKeys")
	public KeySet getKeys() {
		PrimeModel response = primeServiceInterface.getPrime();
		int p = response.number;
		
		response = primeServiceInterface.getPrime();
		int q = response.number;
//		int p = primeServiceInterface.getPrime();
//		int q = primeServiceInterface.getPrime();
		
//		System.out.println("p: "+ p +"\nq: " + q);
		return keyGen.getKeys(p, q, null);
		
	}
	
	/**
	 * returns a PublicKey, PrivateKey, and IdentifyingInformation
	 */
	@WebMethod(action="/getKeysWithIdentifier")
	public KeySet getKeysWithIdentifier(@WebParam(name="identifier") String identifier) {
		PrimeModel response = primeServiceInterface.getPrime();
		int p = response.number;
		
		response = primeServiceInterface.getPrime();
		int q = response.number;
//		int p = primeServiceInterface.getPrime();
//		int q = primeServiceInterface.getPrime();
		
//		System.out.println("p: "+ p +"\nq: " + q);
		return keyGen.getKeys(p, q, identifier);
	}
	

}
