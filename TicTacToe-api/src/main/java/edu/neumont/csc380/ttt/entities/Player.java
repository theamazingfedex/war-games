package edu.neumont.csc380.ttt.entities;

public class Player {
	private long publicKey;
	private boolean isX;
	
	public Player(){
		
	}

	public long getPublicKey() {
		return publicKey;
	}

	public boolean isX() {
		return isX;
	}

	public void setPublicKey(long publicKey) {
		this.publicKey = publicKey;
	}

	public void setX(boolean isX) {
		this.isX = isX;
	}
}
