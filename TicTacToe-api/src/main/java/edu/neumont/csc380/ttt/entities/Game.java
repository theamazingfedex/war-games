package edu.neumont.csc380.ttt.entities;

public class Game {
	private int id;
	private Player X;
	private Player O;
	
	public Game(){
		
	}

	public int getId() {
		return id;
	}

	public Player getX() {
		return X;
	}

	public Player getO() {
		return O;
	}

	public void setId(int id) {
		this.id = id;
	}

	public void setX(Player x) {
		X = x;
	}

	public void setO(Player o) {
		O = o;
	}
	
}
