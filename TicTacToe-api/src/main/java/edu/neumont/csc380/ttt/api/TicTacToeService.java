package edu.neumont.csc380.ttt.api;

public interface TicTacToeService {
	public String newGame();
	public String joinGame();
	public String makeMove();
}
