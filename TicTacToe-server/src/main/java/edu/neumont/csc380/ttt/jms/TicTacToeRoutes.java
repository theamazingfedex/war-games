package edu.neumont.csc380.ttt.jms;

import org.apache.camel.builder.RouteBuilder;
import org.apache.camel.converter.jaxb.JaxbDataFormat;



public class TicTacToeRoutes extends RouteBuilder{
	
	@Override
	public void configure() throws Exception {
		JaxbDataFormat jaxb = new JaxbDataFormat("edu.neumont.csc380.ttt");
		
		from("jms:queue:ttt").to("TicTacToe-Service");
//			.unmarshal(jaxb)
//			.marshal(jaxb);
		
	}

}
