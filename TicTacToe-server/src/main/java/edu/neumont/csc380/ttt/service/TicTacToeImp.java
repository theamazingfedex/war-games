package edu.neumont.csc380.ttt.service;

import org.springframework.stereotype.Service;

import edu.neumont.csc380.ttt.api.TicTacToeService;


@Service("TicTacToe-Service")
public class TicTacToeImp implements TicTacToeService{

	public String newGame() {
		System.out.println("ONE");
		return "newGame()";
	}

	public String joinGame() {
		System.out.println("TWO");
		return "joinGame()";
	}

	public String makeMove() {
		System.out.println("THREE");
		return "makeMove()";
	}

}
