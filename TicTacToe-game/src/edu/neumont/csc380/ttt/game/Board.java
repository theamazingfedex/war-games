package edu.neumont.csc380.ttt.game;

/*
 * TicTacToe Game
 *
 * Author: Brooks
 *
 */

/**
 * This class sets up the board with '0' values
 */
public class Board {
	private int[][] Board = new int[3][3];

	public Board() {
		emptyBoard();
	}

	/**
	 * This method sets all values to 0.
	 */
	public void emptyBoard() {
		for (int row = 0; row < 3; row++)
			for (int column = 0; column < 3; column++)
				Board[row][column] = 0;
	}

	public void printBoard() {
		System.out.println();
		for (int row = 0; row < 3; row++) {

			for (int column = 0; column < 3; column++) {

				if (Board[row][column] == -1) {
					System.out.print(" X ");
				}
				if (Board[row][column] == 1) {
					System.out.print(" O ");
				}
				if (Board[row][column] == 0) {
					System.out.print("   ");
				}
				if (column == 0 || column == 1)
					System.out.print("|");
			}
			System.out.println();
		}
	}

	/**
	 * returns which number (-1, 0, 1) is in that position
	 */
	public int getPosition(int[] attempt) {
		return Board[attempt[0]][attempt[1]];
	}

	/**
	 * changes a location of the board to -1 or 1 depending on the player 
	 */
	public void setPosition(int[] attempt, int player) {
		if (player == 1)
			Board[attempt[0]][attempt[1]] = -1;
		else
			Board[attempt[0]][attempt[1]] = 1;
	}

	public int checkRows() {
		for (int row = 0; row < 3; row++) {

			if ((Board[row][0] + Board[row][1] + Board[row][2]) == -3)
				return -1;
			if ((Board[row][0] + Board[row][1] + Board[row][2]) == 3)
				return 1;
		}
		return 0;
	}

	public int checkColumns() {
		for (int column = 0; column < 3; column++) {

			if ((Board[0][column] + Board[1][column] + Board[2][column]) == -3)
				return -1;
			if ((Board[0][column] + Board[1][column] + Board[2][column]) == 3)
				return 1;
		}
		return 0;
	}

	public int checkDiagonals() {
		if ((Board[0][0] + Board[1][1] + Board[2][2]) == -3)
			return -1;
		if ((Board[0][0] + Board[1][1] + Board[2][2]) == 3)
			return 1;
		if ((Board[0][2] + Board[1][1] + Board[2][0]) == -3)
			return -1;
		if ((Board[0][2] + Board[1][1] + Board[2][0]) == 3)
			return 1;

		return 0;
	}

	/**
	 * checks if the board is full, if any 0 is found then its not full.
	 */
	public boolean full() {
		for (int row = 0; row < 3; row++)
			for (int column = 0; column < 3; column++)
				if (Board[row][column] == 0)
					return false;
		return true;
	}
}
