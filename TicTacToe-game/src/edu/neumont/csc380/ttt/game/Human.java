package edu.neumont.csc380.ttt.game;

import java.util.Scanner;

/*
 * TicTacToe Game
 *
 * Author: Brooks
 *
 */

/**
 * This class contains the human player logic.
 */
public class Human extends Player {
	public Scanner input = new Scanner(System.in);

	public Human(int player) {
		super(player);
		this.player = player;
		System.out.println("Player 'Human' created!");
	}

	@Override
	public void play(Board board) {
		checkSpot(board);
		board.setPosition(attempt, player);
	}

	/**
	 * This method makes sure the move is valid.
	 */
	@Override
	public void checkSpot(Board board) {
		do {
			do {
				System.out.print("Row: ");
				attempt[0] = input.nextInt();

				if (attempt[0] > 3 || attempt[0] < 1)
					System.out.println("Invalid row. It's 1, 2 or 3");

			} while (attempt[0] > 3 || attempt[0] < 1);

			do {
				System.out.print("Column: ");
				attempt[1] = input.nextInt();

				if (attempt[1] > 3 || attempt[1] < 1)
					System.out.println("Invalid column. � 1, 2 or 3");

			} while (attempt[1] > 3 || attempt[1] < 1);

			attempt[0]--;
			attempt[1]--;

			if (!check(attempt, board))
				System.out.println("Occupied. Choose an empty spot.");
		} while (!check(attempt, board));
	}
	
}
