package edu.neumont.csc380.ttt.game;

/*
 * TicTacToe Game
 *
 * Author: Brooks
 *
 */

/**
 * This class exists to implement AI and Human types
 */
public abstract class Player {

	protected int[] attempt = new int[2];
	protected int player;

	public Player(int player) {
		this.player = player;
	}

	public abstract void play(Board board);

	public abstract void checkSpot(Board board);

	public boolean check(int[] attempt, Board board) {
		if (board.getPosition(attempt) == 0)
			return true;
		else
			return false;
	}

}
