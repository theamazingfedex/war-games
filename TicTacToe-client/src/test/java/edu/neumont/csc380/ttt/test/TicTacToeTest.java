package edu.neumont.csc380.ttt.test;

import junit.framework.Assert;

import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.AbstractJUnit4SpringContextTests;

import edu.neumont.csc380.ttt.api.TicTacToeService;
@ContextConfiguration(locations="beans.xml")
public class TicTacToeTest extends AbstractJUnit4SpringContextTests{
	@Autowired TicTacToeService tttService;

	@Test
	public void test(){
		System.out.println("d");
		Assert.assertEquals("newGame()", tttService.newGame());
		System.out.println("c");
		Assert.assertEquals("joinGame()",tttService.joinGame());
		System.out.println("b");
		Assert.assertEquals("makeMove()",tttService.makeMove());
		System.out.println("a");
	}
}
